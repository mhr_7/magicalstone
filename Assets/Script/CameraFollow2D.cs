using UnityEngine;

public class CameraFollow2D : MonoBehaviour
{
    public bool isFollowing;
    public float FollowSpeed = 2f;
    public Transform Target;

    private void Update()
    {
        if (isFollowing)
        {
            Vector3 newPosition = Target.position;
            newPosition.z = -10;
            transform.position = Vector3.Slerp(transform.position, newPosition, FollowSpeed * Time.deltaTime);
        }
    }
}
