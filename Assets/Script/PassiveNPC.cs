﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassiveNPC : MonoBehaviour
{
    [SerializeField]
    public item[] requestedItem;
    public item ownedItem;
    public GameObject questionPanel;
    public bool isFulfilled;

    public GameObject gateToOpen;
    //public bool justInstruction;

    private bool IsShowingTask = false;

    //public bool isARequest;

    public bool[] fulfilled;

    private bool match;
    //private int itt;
    // Start is called before the first frame update
    void Start()
    {
        //imgaeChild = questionPanel.gameObject.GetComponentInChildren<Image>() as GameObject;
        fulfilled = new bool[requestedItem.Length];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowTask(){

    }

    public item GetThisItem(){
      return ownedItem;
    }

    public bool NPCRequest(item m_item){
      match = false;
      int i=0;
      //if(requestedItem.Length>1)
      for(; i<requestedItem.Length;i++){
        if(m_item.name == requestedItem[i].name && !fulfilled[i]){
          match = true;
          break;
        }
        //Debug.Log("Req itt :" + requestedItem[i].name);
      }

      if(match){
        //requestedItem[i] = new item();
        fulfilled[i] = true;
        return true;
      }
      return false;
    }

    public IEnumerator ShowAllTask(){
        if(!IsShowingTask){
            questionPanel.gameObject.GetComponent<Animator>().SetBool("ShowAnim", true);
            for(int i=0; i<requestedItem.Length; i++){
                Debug.Log("inside for");
                questionPanel.transform.GetChild(1).GetComponent<Image>().sprite = requestedItem[i].img;
                yield return new WaitForSeconds(1.5f);
            }
            questionPanel.gameObject.GetComponent<Animator>().SetBool("ShowAnim", false);
            yield return new WaitForEndOfFrame();
            IsShowingTask = false;
        }
    }

    public IEnumerator ShowInstruction()
    {
        if (!IsShowingTask)
        {
            questionPanel.gameObject.GetComponent<Animator>().SetBool("ShowAnim", true);
            for (int i = 0; i < fulfilled.Length; i++)
            {
                //Debug.Log("inside for");
                if (!fulfilled[i])
                {
                    questionPanel.transform.GetChild(1).GetComponent<Image>().sprite = requestedItem[i].img;
                    yield return new WaitForSeconds(1.5f);
                }
            }
            questionPanel.gameObject.GetComponent<Animator>().SetBool("ShowAnim", false);
            yield return new WaitForEndOfFrame();
            IsShowingTask = false;
        }
    }

    public bool CheckFulfill()
    {
        int count = 0;
        for (int i = 0; i < fulfilled.Length; i++)
            if (fulfilled[i] == true) count++;

        Debug.Log("Number of Count: " + count);
        if (count == fulfilled.Length)
        {
            isFulfilled = true;
            if(gateToOpen != null)
                StartCoroutine(OpenGate(1.5f));
            return true;
        }
        return false;
//            isFulfilled = true;
    }

    public IEnumerator OpenGate(float time)
    {
        GameObject mainCam = GameObject.FindGameObjectWithTag("MainCamera");
        //Vector3 temp = mainCam.transform.position;
        Vector3 dest = new Vector3(gateToOpen.transform.position.x, gateToOpen.transform.position.y, mainCam.transform.position.z);
        //mainCam = transform.Find("Main Camera").gameObject;
        mainCam.GetComponent<CameraFollow2D>().isFollowing = false;

        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            mainCam.transform.position = Vector3.Lerp(mainCam.transform.position, dest, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        gateToOpen.gameObject.GetComponent<Animator>().SetBool("IsOpen", true);
        yield return new WaitForSeconds(time);
        mainCam.GetComponent<CameraFollow2D>().isFollowing = true;
    }

    public IEnumerator HideQuestBubble(){
      questionPanel.gameObject.GetComponent<Animator>().SetBool("ShowingTask", false);
      yield return new WaitForEndOfFrame();
      IsShowingTask = false;
    }
}
